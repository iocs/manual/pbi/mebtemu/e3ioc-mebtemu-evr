require "mrfioc2" "2.3.1+6"
require essioc

## Common envs
epicsEnvSet "ENGINEER" "ICS_HWI_WP04"

# [localhost]
## [CHESS] configuration (https://chess.esss.lu.se) and node specific settings
iocshLoad "iocrc.iocsh"

# [common] e3 modules
## pre-settings
epicsEnvSet "IOCNAME" "$(EVRIOC)"
## body
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# [IOC] core
iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=$(PEVR),PCIID=$(PCIID),EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=$(PEVR)"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=$(PEVR),$(EVREVTARGS=)"

# [SIT] plugins
## ICSHWI-6250: SIT: ISrc-010Row:CnPw-U-005 FBIS-DLN01:Ctrl-EVR-01
$(SITMPSPLC="#")dbLoadRecords "$(mrfioc2_DIR)evr-mps-plc-ess.db" "P=$(PEVR)"

# [user] settings - use this file to apply local customization
iocshLoad "$(TOP)/user.iocsh" "P=$(PEVR)"

iocInit

iocshLoad "$(mrfioc2_DIR)/evr.r.iocsh" "P=$(PEVR)"
$(EVRAMC2CLKEN=)iocshLoad "$(mrfioc2_DIR)/evrtclk.r.iocsh" "P=$(PEVR)"
iocshLoad "$(mrfioc2_DIR)/evrdlygen.r.iocsh" "P=$(PEVR),$(EVRDLYGENARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrout.r.iocsh" "P=$(PEVR),$(EVROUTARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrin.r.iocsh" "P=$(PEVR),$(EVRINARGS=)"

