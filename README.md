# e3ioc-mebtemu-evr

- *EVR:* `PBI-EMU02:Ctrl-EVR-101`
- *IOC name:* `PBI-EMU02:SC-IOC-004`
- *LOCATION:* `FEB-050Row:CnPw-U-011`
- *FBS:* `=ESS.ACC.B01.B08.B04.K01.K01.K03`
